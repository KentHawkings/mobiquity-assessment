package com.kenthawkings.mobiquityassessment

import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kenthawkings.mobiquityassessment.data.DataManager
import com.kenthawkings.mobiquityassessment.interactor.DefaultConsumablesInteractor
import com.kenthawkings.mobiquityassessment.model.Price
import com.kenthawkings.mobiquityassessment.model.Product
import com.kenthawkings.mobiquityassessment.ui.ConsumablesViewModel
import com.kenthawkings.mobiquityassessment.util.CurrentThreadExecutor
import com.kenthawkings.mobiquityassessment.util.MainCoroutineRule
import com.kenthawkings.mobiquityassessment.util.MockResponseFileReader
import java.math.BigDecimal
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ConsumableViewModelTest {
    @get:Rule
    val testInstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private val testProduct = Product(
        "coke",
        "Coke",
        "/Coke.jpg",
        "Coca-Cola",
        Price(
            BigDecimal(10),
            "EUR"
        )
    )

    private val mockServer = MockWebServer()

    private lateinit var viewModel: ConsumablesViewModel

    @Before
    fun setup_viewModelAndMockServer() {
        mockServer.start()

        val client = OkHttpClient.Builder()
            .dispatcher(Dispatcher(CurrentThreadExecutor()))
            .build()

        val dataManager = DataManager(mockServer.url("/").toString(), client)
        val interactor = DefaultConsumablesInteractor(dataManager)

        viewModel = ConsumablesViewModel(interactor, mock())
    }

    @Test
    fun viewModel_testInitialState() {
        assert(!viewModel.loading)
        assert(viewModel.currentError == null)
        assert(viewModel.model.value == null)
    }

    @Test
    fun viewModel_loadData_correctSuccessHandling() {
        val reader = MockResponseFileReader("success_response.json")
        assertNotNull(reader.content)

        mockServer.enqueue(MockResponse().apply {
            setResponseCode(200)
            setBody(reader.content)
            setHeader("content-type", "application/json")
        })

        viewModel.loadModel()

        assert(!viewModel.loading)
        assertNotEquals(viewModel.loadingVisibility.value, View.VISIBLE)
        assertNull(viewModel.currentError)
        assertNotEquals(viewModel.errorVisibility.value, View.VISIBLE)
        assertNotNull(viewModel.model.value)
        assertEquals(viewModel.contentVisibility.value, View.VISIBLE)
    }

    @Test
    fun viewModel_loadData_correctErrorHandling() {
        mockServer.enqueue(MockResponse().apply {
            setResponseCode(500)
        })
        // Errors thrown by Retrofit suspend functions CANNOT run synchronously, so we must force
        // a block here: https://jakewharton.com/exceptions-and-proxies-and-coroutines-oh-my/
        runBlocking(Dispatchers.Main) {
            viewModel.loadModel().join()
        }

        assert(!viewModel.loading)
        assertNotEquals(viewModel.loadingVisibility.value, View.VISIBLE)
        assertNotNull(viewModel.currentError)
        assertEquals(viewModel.errorVisibility.value, View.VISIBLE)
        assertNull(viewModel.model.value)
        assertNotEquals(viewModel.contentVisibility.value, View.VISIBLE)
    }

    @Test
    fun viewModel_productSelection() {
        viewModel.onProductSelected(testProduct)

        assertEquals(viewModel.productName.value, testProduct.name)
        assertEquals(
            viewModel.productImageUrl.value,
            testProduct.imageUrl
        )
        viewModel.productPrice.value?.startsWith("€10")?.let { assert(it) }
    }

    @After
    fun tearDown() {
        mockServer.close()
    }
}
