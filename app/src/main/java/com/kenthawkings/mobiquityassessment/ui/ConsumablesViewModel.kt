package com.kenthawkings.mobiquityassessment.ui

import android.view.View
import androidx.annotation.MainThread
import androidx.databinding.BaseObservable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kenthawkings.mobiquityassessment.R
import com.kenthawkings.mobiquityassessment.interactor.ConsumablesInteractor
import com.kenthawkings.mobiquityassessment.model.ConsumableCategory
import com.kenthawkings.mobiquityassessment.model.Product
import com.kenthawkings.mobiquityassessment.ui.ConsumableCategoriesAdapter.Companion.CONSUMABLE_CATEGORY_VIEW_TYPE
import com.kenthawkings.mobiquityassessment.ui.ConsumableCategoriesFragmentDirections.actionConsumableCategoriesFragmentToProductDetailsFragment
import com.kenthawkings.mobiquityassessment.ui.base.BaseViewModel
import com.kenthawkings.mobiquityassessment.util.ConnectivityProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.NumberFormat
import java.util.Currency
import javax.inject.Inject
import kotlinx.coroutines.Job
import timber.log.Timber

@HiltViewModel
class ConsumablesViewModel @Inject constructor(
    private val interactor: ConsumablesInteractor,
    private val connectivityProvider: ConnectivityProvider
) : BaseViewModel<List<ConsumableCategory>>(), OnProductSelectedListener,
    ConnectivityProvider.ConnectivityStateListener {

    init {
        connectivityProvider.addListener(this)
    }

    private val _productName = MutableLiveData<String?>()
    val productName: LiveData<String?>
        get() = _productName

    private val _productImageUrl = MutableLiveData<String?>()
    val productImageUrl: LiveData<String?>
        get() = _productImageUrl

    private val _productPrice = MutableLiveData<String?>()
    val productPrice: LiveData<String?>
        get() = _productPrice

    override fun onStateChange(state: ConnectivityProvider.NetworkState) {
        currentError?.let {
            if (state.hasInternet && !loading) {
                loadModel()
            }
        }
    }

    override fun onProductSelected(product: Product) {
        _productName.value = product.name
        _productImageUrl.value = product.imageUrl
        _productPrice.value = product.price.let {
            val format = NumberFormat.getCurrencyInstance()
            format.maximumFractionDigits = 2
            format.currency = Currency.getInstance(product.price.currency)
            format.format(product.price.amount)
        }
    }

    override fun onCleared() {
        super.onCleared()
        connectivityProvider.removeListener(this)
    }

    fun onClickReload(@Suppress("UNUSED_PARAMETER") view: View) {
        loadModel()
    }

    fun createAdapter() = ConsumableCategoriesAdapter(this)

    fun createLayoutManager(recyclerView: RecyclerView): RecyclerView.LayoutManager {
        val context = recyclerView.context
        val span = context.resources.getInteger(R.integer.consumables_span_size)
        val layoutManager = GridLayoutManager(
            context,
            span
        )
        layoutManager.spanSizeLookup =
            object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int) =
                    if (recyclerView.adapter?.getItemViewType(position) == CONSUMABLE_CATEGORY_VIEW_TYPE) {
                        span
                    } else {
                        1
                    }
            }
        return layoutManager
    }

    fun loadModel(): Job {
        currentError = null
        loading = true
        model.value = null
        return interactor.load(viewModelScope, @MainThread {
            loading = false
            model.value = it
        }, @MainThread {
            loading = false
            currentError = it
            Timber.e(it)
        })
    }
}

class ConsumableCategoryObservable(category: ConsumableCategory) : BaseObservable() {
    val name = category.name
}

class ProductDetailObservable(
    private val product: Product,
    private val onProductSelectedListener: OnProductSelectedListener
) : BaseObservable() {
    val imageUrl = product.imageUrl

    val name = product.name

    fun onClickItem(view: View) {
        onProductSelectedListener.onProductSelected(product)
        val action = actionConsumableCategoriesFragmentToProductDetailsFragment(product.name)
        view.findNavController().navigate(action)
    }
}
