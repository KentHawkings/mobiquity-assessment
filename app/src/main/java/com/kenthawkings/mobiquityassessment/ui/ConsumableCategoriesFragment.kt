package com.kenthawkings.mobiquityassessment.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.kenthawkings.mobiquityassessment.databinding.FragmentConsumableCategoriesBinding
import com.kenthawkings.mobiquityassessment.ui.base.ViewModelFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConsumableCategoriesFragment :
    ViewModelFragment<ConsumablesViewModel, FragmentConsumableCategoriesBinding>() {
    override val viewModel: ConsumablesViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        val adapter = viewModel.createAdapter()
        binding!!.recyclerViewConsumables.adapter = adapter
        binding!!.recyclerViewConsumables.layoutManager =
            viewModel.createLayoutManager(binding!!.recyclerViewConsumables)
        viewModel.model.observe(viewLifecycleOwner, { items ->
            items?.let {
                adapter.setList(it)
                adapter.notifyItemRangeChanged(0, it.size)
            }
        })
        return view
    }
}
