package com.kenthawkings.mobiquityassessment.ui.base

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel

open class BaseViewModel<M> : ViewModel() {
    internal var loading = false
        internal set(value) {
            _loadingVisibility.value = if (value) View.VISIBLE else View.GONE
            field = value
        }

    internal var currentError: Throwable? = null
        internal set(value) {
            _errorVisibility.value = value?.let { View.VISIBLE } ?: View.GONE
            field = value
        }

    var model = MutableLiveData<M?>()

    private var _loadingVisibility = MutableLiveData(View.VISIBLE)
    val loadingVisibility: LiveData<Int>
        get() = _loadingVisibility

    private var _contentVisibility = MutableLiveData(View.INVISIBLE)
    val contentVisibility: LiveData<Int>
        get() = _contentVisibility

    private var _errorVisibility = MutableLiveData(View.INVISIBLE)
    val errorVisibility: LiveData<Int>
        get() = _errorVisibility

    private val modelObserver = Observer<M?> {
        _contentVisibility.value = it?.let { View.VISIBLE } ?: View.GONE
    }

    init {
        model.observeForever(modelObserver)
    }

    override fun onCleared() {
        super.onCleared()
        model.removeObserver(modelObserver)
    }
}
