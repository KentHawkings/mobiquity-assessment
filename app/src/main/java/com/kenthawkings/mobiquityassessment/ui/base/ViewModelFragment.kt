package com.kenthawkings.mobiquityassessment.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.kenthawkings.mobiquityassessment.BR
import javax.inject.Inject

// TODO: Hopefully one day the Android entry points can support type parameters, then we never need
// to subclass this and we can just inject instances of ViewModelFragment directly. For now we make
// abstract to ensure subclassing
abstract class ViewModelFragment<VM : BaseViewModel<*>, B : ViewDataBinding> : Fragment() {
    abstract val viewModel: VM

    @Inject
    lateinit var bindingCreator: BindingCreator<B>

    var binding: B? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = bindingCreator.createBinding(inflater, container, false)
        binding!!.setVariable(BR.viewModel, viewModel)
        binding!!.lifecycleOwner = viewLifecycleOwner
        return binding!!.root
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }
}
