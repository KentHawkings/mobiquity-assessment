package com.kenthawkings.mobiquityassessment.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import com.kenthawkings.mobiquityassessment.R
import com.kenthawkings.mobiquityassessment.databinding.ErrorViewBinding

class ErrorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr) {
    private var textView: TextView

    private var button: Button

    init {
        val layoutInflater = LayoutInflater.from(context)!!
        val binding = ErrorViewBinding.inflate(layoutInflater, this, true)
        textView = binding.textViewErrorButton
        button = binding.textViewErrorButton
    }

    var errorText: CharSequence? = context.getText(R.string.error_load_failed)
        set(value) {
            field = value
            textView.text = value
        }

    var buttonText: CharSequence? = context.getText(R.string.reload)
        set(value) {
            field = value
            button.text = value
        }

    fun setButtonOnClickListener(listener: OnClickListener) {
        button.setOnClickListener(listener)
    }
}
