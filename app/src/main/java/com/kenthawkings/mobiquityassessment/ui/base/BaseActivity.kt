package com.kenthawkings.mobiquityassessment.ui.base

import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.kenthawkings.mobiquityassessment.R
import com.kenthawkings.mobiquityassessment.util.ConnectivityProvider
import javax.inject.Inject

open class BaseActivity : AppCompatActivity(), ConnectivityProvider.ConnectivityStateListener {
    @Inject
    lateinit var connectivityProvider: ConnectivityProvider

    override fun onResume() {
        super.onResume()
        connectivityProvider.addListener(this)
    }

    override fun onStateChange(state: ConnectivityProvider.NetworkState) {
        val rootView = window.decorView.findViewById<View>(android.R.id.content)
        @StringRes val stringRes: Int
        val duration: Int
        when (state.hasInternet) {
            true -> {
                stringRes = R.string.connected
                duration = Snackbar.LENGTH_LONG
            }
            false -> {
                stringRes = R.string.not_connected
                duration = Snackbar.LENGTH_INDEFINITE
            }
        }
        Snackbar.make(rootView, stringRes, duration).show()
    }

    override fun onPause() {
        super.onPause()
        connectivityProvider.removeListener(this)
    }
}
