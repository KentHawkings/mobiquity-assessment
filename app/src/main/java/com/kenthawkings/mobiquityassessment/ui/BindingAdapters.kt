package com.kenthawkings.mobiquityassessment.ui

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

@BindingAdapter(value = ["imageUrl", "placeholder", "thumbnail"], requireAll = false)
fun ImageView.loadImageFromUrl(
    url: String?,
    placeholder: Drawable? = null,
    thumbnail: Boolean = false
) {
    val requestManager = Glide.with(this)
    var requestOptions = RequestOptions()
    placeholder?.let {
        requestOptions = requestOptions.placeholder(it)
    }
    requestManager.applyDefaultRequestOptions(requestOptions)
    var requestBuilder = requestManager.load(url)
    if (thumbnail) {
        requestBuilder = requestBuilder.thumbnail(0.5f)
    }
    requestBuilder.into(this)
}
