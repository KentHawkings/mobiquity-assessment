package com.kenthawkings.mobiquityassessment.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.kenthawkings.mobiquityassessment.BR
import com.kenthawkings.mobiquityassessment.databinding.ListItemConsumableCategoryBinding
import com.kenthawkings.mobiquityassessment.databinding.ListItemProductDetailBinding
import com.kenthawkings.mobiquityassessment.model.ConsumableCategory
import com.kenthawkings.mobiquityassessment.model.Product

interface OnProductSelectedListener {
    fun onProductSelected(product: Product)
}

private interface ListItem {
    val itemViewType: Int

    fun onBindViewHolder(holder: BindingViewHolder<*>, position: Int)
}

class ConsumableCategoriesAdapter(
    private val onProductSelectedListener: OnProductSelectedListener
) :
    RecyclerView.Adapter<BindingViewHolder<*>>() {
    companion object {
        const val CONSUMABLE_CATEGORY_VIEW_TYPE = 0
        const val PRODUCT_DETAIL_VIEW_TYPE = 1
    }

    private val items = mutableListOf<ListItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<*> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val viewHolder: BindingViewHolder<*>? = when (viewType) {
            CONSUMABLE_CATEGORY_VIEW_TYPE -> {
                val binding = ListItemConsumableCategoryBinding.inflate(
                    layoutInflater,
                    parent,
                    false
                )
                BindingViewHolder(binding)
            }
            PRODUCT_DETAIL_VIEW_TYPE -> {
                val binding = ListItemProductDetailBinding.inflate(
                    layoutInflater,
                    parent,
                    false
                )
                BindingViewHolder(binding)
            }
            else -> null // Will not reach
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: BindingViewHolder<*>, position: Int) {
        items[position].onBindViewHolder(holder, position)
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) = items[position].itemViewType

    fun setList(list: List<ConsumableCategory>?) {
        items.clear()
        initItems(list)
    }

    private fun initItems(list: List<ConsumableCategory>?) {
        list?.forEach { category ->
            items.add(object : ListItem {
                override val itemViewType = CONSUMABLE_CATEGORY_VIEW_TYPE

                override fun onBindViewHolder(holder: BindingViewHolder<*>, position: Int) {
                    val observable = ConsumableCategoryObservable(category)
                    holder.setBindingVariable(BR.observable, observable)
                }
            })
            category.products.forEach { product ->
                items.add(object : ListItem {
                    override val itemViewType = PRODUCT_DETAIL_VIEW_TYPE

                    override fun onBindViewHolder(holder: BindingViewHolder<*>, position: Int) {
                        val observable = ProductDetailObservable(product, onProductSelectedListener)
                        holder.setBindingVariable(BR.observable, observable)
                    }
                })
            }
        }
    }
}

class BindingViewHolder<V : ViewDataBinding>(private val binding: V) :
    RecyclerView.ViewHolder(binding.root) {
    fun setBindingVariable(variableId: Int, variable: Any?) {
        binding.setVariable(variableId, variable)
    }
}
