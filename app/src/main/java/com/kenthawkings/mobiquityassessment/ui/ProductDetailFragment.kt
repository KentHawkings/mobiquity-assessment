package com.kenthawkings.mobiquityassessment.ui

import androidx.fragment.app.activityViewModels
import com.kenthawkings.mobiquityassessment.databinding.FragmentProductDetailBinding
import com.kenthawkings.mobiquityassessment.ui.base.ViewModelFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailFragment :
    ViewModelFragment<ConsumablesViewModel, FragmentProductDetailBinding>() {

    override val viewModel: ConsumablesViewModel by activityViewModels()
}
