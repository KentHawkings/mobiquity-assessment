@file:Suppress("DEPRECATION")

package com.kenthawkings.mobiquityassessment.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.ConnectivityManager.CONNECTIVITY_ACTION
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.net.NetworkInfo
import android.net.wifi.WifiManager.EXTRA_NETWORK_INFO
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.RequiresApi

interface ConnectivityProvider {
    interface ConnectivityStateListener {
        fun onStateChange(state: NetworkState)
    }

    fun addListener(listener: ConnectivityStateListener)

    fun removeListener(listener: ConnectivityStateListener)

    fun getNetworkState(): NetworkState

    class NetworkState {
        val hasInternet: Boolean

        constructor() {
            hasInternet = false
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        constructor (capabilities: NetworkCapabilities? = null) {
            hasInternet = capabilities?.hasCapability(NET_CAPABILITY_INTERNET) == true
        }

        constructor(networkInfo: NetworkInfo? = null) {
            hasInternet = networkInfo?.isConnectedOrConnecting == true
        }
    }

    companion object {
        fun create(context: Context): ConnectivityProvider {
            val cm = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                DefaultConnectivityProvider(cm)
            } else {
                LegacyConnectivityProvider(context, cm)
            }
        }
    }
}

abstract class BaseConnectivityProvider : ConnectivityProvider {
    private val handler = Handler(Looper.getMainLooper())
    private val listeners = mutableSetOf<ConnectivityProvider.ConnectivityStateListener>()
    private var subscribed = false

    override fun addListener(listener: ConnectivityProvider.ConnectivityStateListener) {
        listeners.add(listener)
        verifySubscription()
    }

    override fun removeListener(listener: ConnectivityProvider.ConnectivityStateListener) {
        listeners.remove(listener)
        verifySubscription()
    }

    private fun verifySubscription() {
        if (!subscribed && listeners.isNotEmpty()) {
            subscribe()
            subscribed = true
        } else if (subscribed && listeners.isEmpty()) {
            unsubscribe()
            subscribed = false
        }
    }

    protected fun dispatchChange(state: ConnectivityProvider.NetworkState) {
        handler.post {
            for (listener in listeners) {
                listener.onStateChange(state)
            }
        }
    }

    protected abstract fun subscribe()

    protected abstract fun unsubscribe()
}

@RequiresApi(Build.VERSION_CODES.N)
class DefaultConnectivityProvider(private val cm: ConnectivityManager) :
    BaseConnectivityProvider() {

    private val networkCallback = ConnectivityCallback()

    override fun subscribe() {
        cm.registerDefaultNetworkCallback(networkCallback)
    }

    override fun unsubscribe() {
        cm.unregisterNetworkCallback(networkCallback)
    }

    override fun getNetworkState() =
        ConnectivityProvider.NetworkState(cm.getNetworkCapabilities(cm.activeNetwork))

    private inner class ConnectivityCallback : ConnectivityManager.NetworkCallback() {
        override fun onCapabilitiesChanged(network: Network, capabilities: NetworkCapabilities) {
            dispatchChange(ConnectivityProvider.NetworkState(capabilities))
        }

        override fun onLost(network: Network) {
            dispatchChange(ConnectivityProvider.NetworkState())
        }
    }
}

class LegacyConnectivityProvider(
    private val context: Context,
    private val cm: ConnectivityManager
) : BaseConnectivityProvider() {

    private val receiver = ConnectivityReceiver()

    override fun subscribe() {
        context.registerReceiver(receiver, IntentFilter(CONNECTIVITY_ACTION))
    }

    override fun unsubscribe() {
        context.unregisterReceiver(receiver)
    }

    override fun getNetworkState() = ConnectivityProvider.NetworkState(cm.activeNetworkInfo)

    private inner class ConnectivityReceiver : BroadcastReceiver() {
        override fun onReceive(c: Context, intent: Intent) {
            // on some devices ConnectivityManager.getActiveNetworkInfo() does not provide the correct network state
            // https://issuetracker.google.com/issues/37137911
            val networkInfo = cm.activeNetworkInfo
            val fallbackNetworkInfo: NetworkInfo? = intent.getParcelableExtra(EXTRA_NETWORK_INFO)
            // a set of dirty workarounds
            val state: ConnectivityProvider.NetworkState =
                if (networkInfo?.isConnectedOrConnecting == true) {
                    ConnectivityProvider.NetworkState(networkInfo)
                } else if (networkInfo != null && fallbackNetworkInfo != null &&
                    networkInfo.isConnectedOrConnecting != fallbackNetworkInfo.isConnectedOrConnecting
                ) {
                    ConnectivityProvider.NetworkState(fallbackNetworkInfo)
                } else {
                    val state = networkInfo ?: fallbackNetworkInfo
                    if (state != null) ConnectivityProvider.NetworkState(
                        state
                    ) else ConnectivityProvider.NetworkState()
                }
            dispatchChange(state)
        }
    }
}
