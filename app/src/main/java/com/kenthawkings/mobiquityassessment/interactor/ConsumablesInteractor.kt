package com.kenthawkings.mobiquityassessment.interactor

import com.kenthawkings.mobiquityassessment.data.DataManager
import com.kenthawkings.mobiquityassessment.model.ConsumableCategory
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

interface ConsumablesInteractor {
    fun load(
        scope: CoroutineScope,
        onSuccess: (List<ConsumableCategory>) -> Unit,
        onError: (Throwable) -> Unit
    ): Job
}

class DefaultConsumablesInteractor(
    private val dataManager: DataManager
) : ConsumablesInteractor {
    override fun load(
        scope: CoroutineScope,
        onSuccess: (List<ConsumableCategory>) -> Unit,
        onError: (Throwable) -> Unit
    ): Job {
        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            onError(throwable)
        }
        return scope.launch(exceptionHandler) {
            onSuccess(dataManager.getConsumableCategories())
        }
    }
}
