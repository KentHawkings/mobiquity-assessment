package com.kenthawkings.mobiquityassessment.injection.module

import android.view.LayoutInflater
import android.view.ViewGroup
import com.kenthawkings.mobiquityassessment.databinding.FragmentConsumableCategoriesBinding
import com.kenthawkings.mobiquityassessment.databinding.FragmentProductDetailBinding
import com.kenthawkings.mobiquityassessment.ui.base.BindingCreator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
class FragmentModule {
    @Provides
    fun consumablesCategoryBindingCreator() =
        object : BindingCreator<FragmentConsumableCategoriesBinding> {
            override fun createBinding(
                inflater: LayoutInflater,
                viewGroup: ViewGroup?,
                attachedToParent: Boolean
            ): FragmentConsumableCategoriesBinding {
                return FragmentConsumableCategoriesBinding.inflate(
                    inflater,
                    viewGroup,
                    attachedToParent
                )
            }
        }

    @Provides
    fun productDetailBindingCreator() = object : BindingCreator<FragmentProductDetailBinding> {
        override fun createBinding(
            inflater: LayoutInflater,
            viewGroup: ViewGroup?,
            attachedToParent: Boolean
        ): FragmentProductDetailBinding {
            return FragmentProductDetailBinding.inflate(inflater, viewGroup, attachedToParent)
        }
    }
}
