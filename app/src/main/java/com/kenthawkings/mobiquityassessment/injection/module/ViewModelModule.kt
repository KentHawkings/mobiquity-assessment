package com.kenthawkings.mobiquityassessment.injection.module

import com.kenthawkings.mobiquityassessment.data.DataManager
import com.kenthawkings.mobiquityassessment.interactor.ConsumablesInteractor
import com.kenthawkings.mobiquityassessment.interactor.DefaultConsumablesInteractor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class ViewModelModule {
    @Provides
    @ViewModelScoped
    fun consumableInteractor(dataManager: DataManager): ConsumablesInteractor =
        DefaultConsumablesInteractor(dataManager)
}
