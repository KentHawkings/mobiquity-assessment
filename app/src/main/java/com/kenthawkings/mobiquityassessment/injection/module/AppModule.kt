package com.kenthawkings.mobiquityassessment.injection.module

import android.app.Application
import com.kenthawkings.mobiquityassessment.data.DataManager
import com.kenthawkings.mobiquityassessment.util.ConnectivityProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    fun endpoint() = "http://mobcategories.s3-website-eu-west-1.amazonaws.com/"

    @Provides
    fun client() =
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor { message ->
                println(message)
                Timber.tag("OkHttp").d(message)
            }.setLevel(HttpLoggingInterceptor.Level.BODY))
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()

    @Provides
    @Singleton
    fun dataManager(endpoint: String, client: OkHttpClient) = DataManager(endpoint, client)

    @Provides
    @Singleton
    fun connectivityProvider(app: Application) = ConnectivityProvider.create(app)
}
