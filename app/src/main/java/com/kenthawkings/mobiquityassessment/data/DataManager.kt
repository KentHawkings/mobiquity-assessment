package com.kenthawkings.mobiquityassessment.data

import androidx.annotation.WorkerThread
import com.kenthawkings.mobiquityassessment.data.remote.ServiceProvider
import okhttp3.OkHttpClient

class DataManager(endpoint: String, client: OkHttpClient) {
    private val services = ServiceProvider(endpoint, client)

    @WorkerThread
    suspend fun getConsumableCategories() = services.consumableService.getConsumables()
}
