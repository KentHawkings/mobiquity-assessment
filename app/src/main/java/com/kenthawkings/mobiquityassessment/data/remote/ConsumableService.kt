package com.kenthawkings.mobiquityassessment.data.remote

import com.kenthawkings.mobiquityassessment.model.ConsumableCategory
import retrofit2.http.GET

interface ConsumableService {
    @GET("/")
    suspend fun getConsumables(): List<ConsumableCategory>
}
