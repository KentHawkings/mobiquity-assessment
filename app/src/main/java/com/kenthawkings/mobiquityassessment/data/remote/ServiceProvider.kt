package com.kenthawkings.mobiquityassessment.data.remote

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServiceProvider(endpoint: String, client: OkHttpClient) {
    val consumableService: ConsumableService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(endpoint)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        consumableService = retrofit.create(ConsumableService::class.java)
    }
}
