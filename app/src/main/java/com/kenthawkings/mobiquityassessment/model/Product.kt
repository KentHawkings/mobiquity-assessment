package com.kenthawkings.mobiquityassessment.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class Price(
    @SerializedName("amount") val amount: BigDecimal,
    @SerializedName("currency") val currency: String
)

data class Product(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("url") val imagePath: String?,
    @SerializedName("description") val description: String?,
    @SerializedName("salePrice") val price: Price
) {
    val imageUrl: String?
        get() = imagePath?.let {
            "http://mobcategories.s3-website-eu-west-1.amazonaws.com$it"
        }
}
