package com.kenthawkings.mobiquityassessment.model

import com.google.gson.annotations.SerializedName

data class ConsumableCategory(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String?,
    @SerializedName("products") val products: List<Product>
)
